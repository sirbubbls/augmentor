import scipy as sp
import numpy as np
import cv2 as cv


class Augmentor:
    def __init__(self):
        pass

    def flip(self, image, direction):
        if direction == 'horizontal':
            return np.fliplr(image)
        elif direction == 'vertical':
            return np.flipud(image)

        raise Exception('Keyword \'direction=' + direction +
                        '\' is unknown use horizontal/vertical')

    def noise(self, image, strength=0.1):
        width, height = image.shape
        noise = np.random.normal(size=(width, height))

        return image + noise * strength

    def rotate(self, image, degree):
        width, height = image.shape
        rotational_matrix = cv.getRotationMatrix2D(
            (int(width / 2), int(height / 2)), degree, 1)

        return cv.warpAffine(image, rotational_matrix, (height, width))
